<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::post('/quote', 'QuoteController@postQuote');
//Route::get('/quotes', 'QuoteController@getQuote');

# GET
Route::get('/getstudents', 'StudentsController@getStudents');
Route::get('/getfaculty', 'StudentsController@getFaculty');
Route::get('/getcourse', 'StudentsController@getCourse');
Route::get('/getgroup', 'StudentsController@getGroup');


# POST
Route::post('/postfaculty', 'StudentsController@postFaculty');
Route::post('/postcourse', 'StudentsController@postCourse');
Route::post('/postgroup', 'StudentsController@postGroup');
Route::post('/poststudents', 'StudentsController@postStudents');
Route::post('/deletestudents', 'StudentsController@deleteStudent');
Route::post('/updatetudents', 'StudentsController@UpdateStudentData');
Route::post('/selectcourse', 'StudentsController@SelectCourse');

