<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grup extends Model
{
    protected $table = 'grups';

    public function course()
    {
        return $this->belongsTo('App\Course');
    }

    public function students()
    {
        return $this->hasMany('App\Student','group_id');
    }
}
