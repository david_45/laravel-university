<?php

namespace App\Http\Controllers;

use App\Qoute;
use Illuminate\Http\Request;

class  QuoteController extends Controller
{
    public function postQuote(Request $request)
    {
        $qoute = new Qoute();
        $qoute->content = $request->input('content');
        $qoute->save();
        return response()->json(['content' => $qoute], 201);
    }

    public function getQuote()
    {
        $qoutes = Qoute::all();
        $response = [
            'content' => $qoutes
        ];
        return response()->json($response,200);
    }
}