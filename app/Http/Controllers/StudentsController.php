<?php

namespace App\Http\Controllers;

use App\Faculty;
use App\Course;
use App\Grup;
use App\Student;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Validator;


class StudentsController extends Controller
{

    public function postFaculty(Request $request)
    {
//        dd($request->all());
        $fac = new Faculty();
        $fac->name = $request->input('name');
        $fac->save();
        return response()->json(['name' => $fac], 201);
    }

    public function getFaculty()
    {
        $faculty = Faculty::all();

        $response = [
            'faculty' => $faculty
        ];
        return response()->json($response, 200);

    }

    public function postCourse(Request $request)
    {
        $course = new Course();
        $course->name = $request->input('name');
        $course->faculty_id = $request->input('faculty_id');
        $course->save();
        return response()->json(['course' => $course], 201);

    }

    public function getCourse()
    {
        $course = Course::all();

        $response = [
            'course' => $course
        ];
        return response()->json($response, 200);
    }

    public function postGroup(Request $request)
    {
        $group = new Grup();
        $group->name = $request->input('name');
        $group->course_id = $request->input('course_id');
        $group->save();
        return response()->json(['group' => $group], 201);

    }

    public function getGroup()
    {
        $group = Grup::all();

        $response = [
            'group' => $group
        ];
        return response()->json($response, 200);
    }

    public function postStudents(Request $request)
    {
        $validator=Validator::make($request->all(),
            [
            'name'=>'required',
            'surname'=>'required',
            'patronymic'=>'required',
            'phone'=>'required',
            'address'=>'required',
            'address'=>'required',
            ]);
        if($validator->fails()) {
            return response()->json(['errors' => $validator->messages()], 201);
        }


        $students = new Student();
        $students->name = $request->input('name');
        $students->surname = $request->input('surname');
        $students->patronymic = $request->input('patronymic');
        $students->phone = $request->input('phone');
        $students->address = $request->input('address');
        $students->group_id = $request->input('group_id');
        $students->course_id = $request->input('course_id');
        $students->faculties_id = $request->input('faculties_id');
        $students->save();
        return response()->json(['students' => $students], 201);

    }

    public function getStudents()
    {
        $students = Student::all();

        $students = [
            'students' => $students
        ];
        return response()->json($students, 200);
    }

    public function deleteStudent(Request $request)
    {
        $students = Student::find($request->input('id'));
        $students->delete();

        return response()->json(['students' => $students], 201);
    }


    public function UpdateStudentData(Request $request)
    {
        $array = [
            'name' => $request->input('name'),
            'surname' => $request->input('surname'),
            'patronymic' => $request->input('patronymic'),
            'phone' => $request->input('phone'),
            'address' => $request->input('address'),
            'group_id' => $request->input('group_id'),
            'course_id' => $request->input('course_id'),
            'faculties_id' => $request->input('faculties_id')
        ];
        DB::table('students')
            ->where('id', $request->input('id'))
            ->update($array);

        $students = Student::all();

        return response()->json(['students' => $students], 201);
    }

    public function SelectCourse(Request $request)
    {
        $allStudents = [];
        $couse = Course::find(7);
        $group = $couse->group;

//        foreach ($group as $gro) {
//            $groups = Grup::find($gro->id);
//            $allStudents[]= $groups->students;
//        }
//        return response()->json(['students' => $allStudents], 201);

    }


}
