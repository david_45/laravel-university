<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';

    public function group()
    {
        return $this->hasMany('App\Grup','course_id');
    }
}
